import {createStackNavigator} from 'react-navigation-stack'
import {createDrawerNavigator} from 'react-navigation-drawer'
import {createAppContainer, createNavigationContainer} from 'react-navigation'
import Dashboard from '../ui/Dashboard'
import FoodList from '../ui/foodlist'
import Favorites from '../ui/Favourites'

const screens={
    home:{
        screen:Dashboard
    },
    foodlist:{
        screen:FoodList
    },
    favourite:{ screen:Favorites}

 
}
const DashboardStack=createStackNavigator(screens,{headerLayoutPreset:'center'})
const FavouriteStack=createStackNavigator({favourite:{ screen:Favorites}},{headerLayoutPreset:'center'})
const MenuNavigation=createDrawerNavigator(
    {
        Home: DashboardStack,
        Favourite:{
            screen:FavouriteStack,
            navigationOptions: {
                drawerLabel: 'Food'
              }
        }
        
    }
);

export default createAppContainer(MenuNavigation)
