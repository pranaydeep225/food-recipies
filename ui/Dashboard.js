import React,{ Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity,ToastAndroid, StatusBar, Image } from 'react-native';
import {Card,Header,Icon, colors} from 'react-native-elements';
import {createStackNavigator} from 'react-navigation-stack'
import {createAppContainer, createNavigationContainer} from 'react-navigation'
import { HeaderButton,HeaderButtons,Item } from 'react-navigation-header-buttons';
import { color } from "react-native-reanimated";

class Dashboard extends Component
{
   static navigationOptions=({navigation})=>
    {
        return{
            title:navigation.getParam('Title','Dashboard'),
            headerStyle:{
            backgroundColor:navigation.getParam('BackgroundColor','#0D2BF1')
            },
            headerTintColor:navigation.getParam('HeaderTintColor','#ffffff'),
           headerLeft: <Icon name='menu' color='#fff'  onPress={() => navigation.openDrawer()} />
        }
    }
    
    apply_color=()=>{
        this.props.navigation.setParams(
            {
                Title:'DashBoard',
                BackgroundColor:'#d100ff'
            }
        );
    }
    render()
    {
       
        return(
           
        <View style={styles.dashboardtotal} >
            <StatusBar backgroundColor='#19d9fd'></StatusBar>
            {/* <Header
                    leftComponent={<Icon name="menu" onPress={() => this.props.navigation.openDrawer()} />}
                /> */}
            <View  style={{flexDirection:'row'}}>
            <View width='40%' ><TouchableOpacity onPress={()=>this._onPress('Veg')} style={styles.cardstyle}><Text style={styles.textstyle}>Veg</Text></TouchableOpacity></View>
            <View  width='40%'><TouchableOpacity onPress={()=>this._onPress('Non-veg')} style={styles.cardstyle}><Text style={styles.textstyle}>Non-Veg</Text></TouchableOpacity></View>
            </View>
            <View style={{flexDirection:'row'}}>
            <View  width='40%'><TouchableOpacity onPress={()=>this._onPress('Breakfast')} style={styles.cardstyle}><Text style={styles.textstyle}>Breakfast</Text></TouchableOpacity></View>
            <View  width='40%'><TouchableOpacity onPress={()=>this._onPress('Snacks')} style={styles.cardstyle}><Text style={styles.textstyle}>Snacks</Text></TouchableOpacity></View>
            </View>
         
        </View>
        );
    }
    _onPress(types){
      //  console.log(types)
   this.props.navigation.navigate('foodlist',{type:types})
    }
}

const styles=StyleSheet.create(
    {
    dashboardtotal:
    {
        flex:1,
    flexDirection:'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent:'space-evenly',
    alignSelf:'stretch'
    },
    textstyle:
    {
    
        color:'#0D2BF1',
        fontSize:20,
        justifyContent:'center',
        alignSelf:'center',
        textAlign:'center',
        textAlignVertical:'center'
    },
    cardstyle:
    {
        borderRadius:10,
        borderColor:"#ff6100",
        backgroundColor:"#ff6100",
        width:'96%',
        paddingTop:"40%",
        paddingBottom:"40%",
        shadowColor:"#e1e1e1",
        shadowOpacity:1,
        shadowOffset:
        {
            width:3,
            height:3
        }

    }
    }
);
export default Dashboard