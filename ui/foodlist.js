import React,{ Component } from "react";
import { StyleSheet, Text, View,FlatList,List, Image,TouchableOpacity,ToastAndroid, Button } from 'react-native';
import { TouchableHighlight } from "react-native-gesture-handler";
import { SafeAreaView } from "react-navigation";
import Constants from 'expo-constants';

class FoodList extends Component
{
   state={
     foods:[]
   } 
   static navigationOptions=({navigation})=>
    {
        return{
            title:navigation.getParam('Title',navigation.state.params.type),
            headerStyle:{
            backgroundColor:navigation.getParam('BackgroundColor','#0D2BF1')
            },
            headerTintColor:navigation.getParam('HeaderTintColor','#ffffff')
        }
    }
    componentWillMount()
    {
      if(this.props.navigation.state.params.type==='Veg')
      {
      const foods=require('../jsons/veg.json').fooditems
      //console.log(foods)
      this.setState({foods});
      }
      if(this.props.navigation.state.params.type==='Non-veg')
      {
      const foods=require('../jsons/nonveg.json').fooditems
      //console.log(foods)
      this.setState({foods});
      }
      if(this.props.navigation.state.params.type==='Breakfast')
      {
      const foods=require('../jsons/breakfast.json').fooditems
      //console.log(foods)
      this.setState({foods});
      }
      if(this.props.navigation.state.params.type==='Snacks')
      {
      const foods=require('../jsons/snacks.json').fooditems
      //console.log(foods)
      this.setState({foods});
      }
      
    }
  
    render()
    {
      console.log(this.state.foods)
        return(
             <SafeAreaView style={styles.viewstyle}> 
        <FlatList style={{flex:1,marginTop:Constants.statusBarHeight}}
            data={this.state.foods}
        renderItem={({item})=><Item itemdetail={item}/>}
        keyExtractor={item=>item.id}
        />
        
        
        </SafeAreaView>
        
        );
    }
}
function Item({itemdetail}) {
  
    return (
        <View style={styles.cardstyle}>
        <TouchableOpacity>
            <View style={{flexDirection:'row'}}>
                <Image style={{width:60,height:60,alignSelf:'center'}} 
                source={{
                    uri: itemdetail.images[0],
                  }}
                />
                <View style={{flexDirection:'column',marginStart:10}}>
                <Text style={styles.titlestyle}>{itemdetail.title}</Text>
                <Text style={styles.textstyle}>{itemdetail.desc}</Text>
                </View>

                
            </View>
        
        </TouchableOpacity>
        </View>
      
    );
  }

const styles=StyleSheet.create(
    {
    viewstyle:
    {
        flex:1,
    flexDirection:'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent:'space-evenly',
    alignSelf:'stretch',
    },
    titlestyle:
    {
        color:'#e1e1e1',
        fontSize:20,
        fontWeight:"700",
        alignSelf:'stretch',
    },
    textstyle:{
        color:'#e1e1e1',
        fontSize:12,
        alignSelf:'stretch',
    },
   cardstyle: {
        borderRadius:10,
        padding:10,
        borderColor:"#e1e1e1",
        borderWidth:3,
        backgroundColor:"#fff",
        width:'96%',
        marginLeft:10,
        marginRight:10,
        shadowColor:"#e1e1e1",
        alignSelf:'stretch',
        shadowOpacity:1,
        shadowOffset:
        {
            width:3,
            height:3
        }

    }

});
export default FoodList